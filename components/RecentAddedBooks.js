import { Image } from "react-bootstrap"
import styles from "../styles/RecentAddedBooks.module.css"

function RecentAddedBooks() {
    return (
        <div className={styles.recentAddedBooksContainer}>
            <h className={styles.recentBooksTitle}>Recent Uploads</h>
            <div className={styles.recentBooks}>
                <div className={styles.images}>
                    <Image className={styles.recentBook} src='https://inkinmytea.files.wordpress.com/2011/12/apj.jpg?w=640' alt='' width={180} height={250}></Image>
                    <Image className={styles.recentBook} src='https://images-na.ssl-images-amazon.com/images/I/91VokXkn8hL.jpg' alt='' width={180} height={250}></Image>
                    <Image className={styles.recentBook} src='https://images-na.ssl-images-amazon.com/images/I/81-QB7nDh4L.jpg' alt='' width={180} height={250}></Image>
                    <Image className={styles.recentBook} src='https://images-na.ssl-images-amazon.com/images/I/71m-MxdJ2WL.jpg' alt='' width={180} height={250}></Image>
                    <Image className={styles.recentBook} src='https://images-na.ssl-images-amazon.com/images/I/71t4GuxLCuL.jpg' alt='' width={180} height={250}></Image>
                    <Image className={styles.recentBook} src='https://19en282jw7pc3zpwj22pg8v0-wpengine.netdna-ssl.com/wp-content/uploads/2021/01/Good-to-Great-Jim-Collins.jpg' alt='' width={180} height={250}></Image>
                    <Image className={styles.recentBook} src='https://images-na.ssl-images-amazon.com/images/I/81mXQdi5x+L.jpg' alt='' width={180} height={250}></Image>
                    <Image className={styles.recentBook} src='https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1498813353l/34267304.jpg' alt='' width={180} height={250}></Image>
                    <Image className={styles.recentBook} src='https://d1csarkz8obe9u.cloudfront.net/posterpreviews/action-thriller-book-cover-design-template-3675ae3e3ac7ee095fc793ab61b812cc_screen.jpg?ts=1588152105' alt='' width={180} height={250}></Image>
                </div>
                <div className={styles.images}>
                    <Image className={styles.recentBook} src='https://inkinmytea.files.wordpress.com/2011/12/apj.jpg?w=640' alt='' width={180} height={250}></Image>
                    <Image className={styles.recentBook} src='https://images-na.ssl-images-amazon.com/images/I/91VokXkn8hL.jpg' alt='' width={180} height={250}></Image>
                    <Image className={styles.recentBook} src='https://images-na.ssl-images-amazon.com/images/I/81-QB7nDh4L.jpg' alt='' width={180} height={250}></Image>
                    <Image className={styles.recentBook} src='https://images-na.ssl-images-amazon.com/images/I/71m-MxdJ2WL.jpg' alt='' width={180} height={250}></Image>
                    <Image className={styles.recentBook} src='https://images-na.ssl-images-amazon.com/images/I/71t4GuxLCuL.jpg' alt='' width={180} height={250}></Image>
                    <Image className={styles.recentBook} src='https://19en282jw7pc3zpwj22pg8v0-wpengine.netdna-ssl.com/wp-content/uploads/2021/01/Good-to-Great-Jim-Collins.jpg' alt='' width={180} height={250}></Image>
                    <Image className={styles.recentBook} src='https://images-na.ssl-images-amazon.com/images/I/81mXQdi5x+L.jpg' alt='' width={180} height={250}></Image>
                    <Image className={styles.recentBook} src='https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1498813353l/34267304.jpg' alt='' width={180} height={250}></Image>
                    <Image className={styles.recentBook} src='https://d1csarkz8obe9u.cloudfront.net/posterpreviews/action-thriller-book-cover-design-template-3675ae3e3ac7ee095fc793ab61b812cc_screen.jpg?ts=1588152105' alt='' width={180} height={250}></Image>
                </div>
            </div>
        </div>
    )
}

export default RecentAddedBooks

