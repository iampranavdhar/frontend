import { useState } from 'react'
import Link from 'next/link'
import styles from "../styles/Header.module.css"
import MenuIcon from '@material-ui/icons/Menu';
import ClearIcon from '@material-ui/icons/Clear';
import { Image } from 'react-bootstrap';

function Header() {

    const [menutoggle, setMenutoggle] = useState(false)

    const Toggle = () => {
        setMenutoggle(!menutoggle)
    }

    /* Close Menu on Click */
    const closeMenu = () => {
        setMenutoggle(false)
    }

    return (
        <div className={styles.header}>
            {/* Logo */}
            <div >
                <Link href='/'>
                    <Image
                        className={styles.logoSymbol}
                        src="/newlogo.png"
                        alt=""
                        width={80}
                        height={50}
                    />
                </Link>
            </div>
            {/* Header Options */}
            <div className={styles.navRight}>
                <input className={styles.searchInput} type='text' placeholder='Search a Book' />
                <ul className={`${menutoggle ? styles.navOptionsActive : styles.navOptions}`}>
                    <li className={styles.option} onClick={() => { closeMenu() }}>
                        <Link href='/'>
                            <a href="#home">Home</a>
                        </Link>
                    </li>
                    <li className={styles.option} onClick={() => { closeMenu() }}>
                        <Link href='/'>
                            <a href="#books">Books</a>
                        </Link>
                    </li>
                    <li className={styles.option} onClick={() => { closeMenu() }}>
                        <Link href='/'>
                            <a href='signin'>SignIn</a>
                        </Link>
                    </li>
                </ul>
            </div>
            {/* MobileMenu Icon */}
            <div className={styles.mobileMenu} onClick={() => { Toggle() }}>
                {menutoggle ? (
                    <ClearIcon className={styles.menuIcon} style={{ fontSize: 40 }} />
                ) : (
                    <MenuIcon className={styles.menuIcon} style={{ fontSize: 40 }} />
                )}
            </div>
        </div>
    )
}

export default Header

