import styles from '../styles/WelcomeBox.module.css'

function WelcomeBox() {
    return (
        <div className={styles.welcomeBox}>
            <p className={styles.welcomeTitle}>WELCOME TO LIBRARY</p>
            <p className={styles.welcomeMessage}>Feed Your Brain</p>
            <p className={styles.welcomeSubmessage}>Grab A Book To Read</p>
        </div>
    )
}

export default WelcomeBox

