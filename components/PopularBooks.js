import { Image } from 'react-bootstrap'
import styles from '../styles/PopularBooks.module.css'

function PopularBooks() {
    return (
        <div className={styles.popularBooksContainer}>
            <h className={styles.popularBooksTitle}>Popular Books</h>
            <div className={styles.popularBooks}>
                <div className={styles.popularBookImages}>
                    <Image className={styles.popularBook} src='https://i2.wp.com/bookjelly.com/wp-content/uploads/2019/05/mannantech2.jpg?resize=206%2C336&ssl=1' alt='' width={180} height={250}></Image>
                    <Image className={styles.popularBook} src='https://rukminim1.flixcart.com/image/612/612/ju79hu80/book/9/5/8/kaleidoscope-original-imaffdvwyztgzufq.jpeg?q=70' alt='' width={180} height={250}></Image>
                    <Image className={styles.popularBook} src='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRS34iIDoKVXOhKhdwsiGSLc9RJmtq_lSQDig&usqp=CAU' alt='' width={180} height={250}></Image>
                    <Image className={styles.popularBook} src='https://i1.wp.com/bookjelly.com/wp-content/uploads/2019/06/mannantech2-2.jpg?resize=204%2C307&ssl=1' alt='' width={180} height={250}></Image>
                    <Image className={styles.popularBook} src='https://i.insider.com/555b87006da811fe218b4568?width=1000&format=jpeg&auto=webp' alt='' width={180} height={250}></Image>
                    <Image className={styles.popularBook} src='https://cdn.geekwire.com/wp-content/uploads/2019/05/Screen-Shot-2019-05-23-at-7.06.40-PM.png' alt='' width={180} height={250}></Image>
                    <Image className={styles.popularBook} src='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRfRHNwRyPkTxnMOzOvv5dOK4OS_lq4-2Yugg&usqp=CAU' alt='' width={180} height={250}></Image>
                    <Image className={styles.popularBook} src='https://d28hgpri8am2if.cloudfront.net/book_images/onix/cvr9781416544067/the-nature-of-technology-9781416544067_hr.jpg' alt='' width={180} height={250}></Image>
                    <Image className={styles.popularBook} src='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ7ElcNy_v2Ri1H3VhYjYP1MzR6zBUwFQWbOirCkaqcfOqJnbrK5ZvdZNUwEfrlmJwn7pA&usqp=CAU' alt='' width={180} height={250}></Image>
                </div>
                <div className={styles.popularBookImages}>
                    <Image className={styles.popularBook} src='https://i2.wp.com/bookjelly.com/wp-content/uploads/2019/05/mannantech2.jpg?resize=206%2C336&ssl=1' alt='' width={180} height={250}></Image>
                    <Image className={styles.popularBook} src='https://rukminim1.flixcart.com/image/612/612/ju79hu80/book/9/5/8/kaleidoscope-original-imaffdvwyztgzufq.jpeg?q=70' alt='' width={180} height={250}></Image>
                    <Image className={styles.popularBook} src='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRS34iIDoKVXOhKhdwsiGSLc9RJmtq_lSQDig&usqp=CAU' alt='' width={180} height={250}></Image>
                    <Image className={styles.popularBook} src='https://i1.wp.com/bookjelly.com/wp-content/uploads/2019/06/mannantech2-2.jpg?resize=204%2C307&ssl=1' alt='' width={180} height={250}></Image>
                    <Image className={styles.popularBook} src='https://i.insider.com/555b87006da811fe218b4568?width=1000&format=jpeg&auto=webp' alt='' width={180} height={250}></Image>
                    <Image className={styles.popularBook} src='https://cdn.geekwire.com/wp-content/uploads/2019/05/Screen-Shot-2019-05-23-at-7.06.40-PM.png' alt='' width={180} height={250}></Image>
                    <Image className={styles.popularBook} src='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRfRHNwRyPkTxnMOzOvv5dOK4OS_lq4-2Yugg&usqp=CAU' alt='' width={180} height={250}></Image>
                    <Image className={styles.popularBook} src='https://d28hgpri8am2if.cloudfront.net/book_images/onix/cvr9781416544067/the-nature-of-technology-9781416544067_hr.jpg' alt='' width={180} height={250}></Image>
                    <Image className={styles.popularBook} src='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ7ElcNy_v2Ri1H3VhYjYP1MzR6zBUwFQWbOirCkaqcfOqJnbrK5ZvdZNUwEfrlmJwn7pA&usqp=CAU' alt='' width={180} height={250}></Image>
                </div>
            </div>
        </div>
    )
}

export default PopularBooks

