import styles from "../styles/Footer.module.css"
import TwitterIcon from '@material-ui/icons/Twitter';
import LinkedInIcon from '@material-ui/icons/LinkedIn';
import TelegramIcon from '@material-ui/icons/Telegram';
import InstagramIcon from '@material-ui/icons/Instagram';

function Footer() {
    return (
        <div className={styles.footer}>
            <div>
                <div className={styles.footerData}>
                    <div className={styles.contactDetails}>
                        <h1>Contact Us</h1>
                        <p>Librarian</p>
                        <p>Government School</p>
                        <p>Visakhapatnam-530041</p>
                        <p>Andhra Pradesh</p>
                        <p>India</p>
                        <p><b>Email:</b>example@gmail.com</p>
                    </div>
                    <div className={styles.usefullLinks}>
                        <h1>Usefull Links</h1>
                        <a href='#home'>Link-1</a>
                        <a href='#home'>Link-1</a>
                        <a href='#home'>Link-1</a>
                        <a href='#home'>Link-1</a>
                    </div>
                    <div className={styles.librarianDetails}>
                        <h1>Librarian</h1>
                        <p>Name</p>
                        <p>Education</p>
                        <p>Contact: +91 9123456787</p>
                    </div>
                </div>
                <div className={styles.contactSocial} >
                    <a href='#home' className={styles.socialIcon}><TwitterIcon style={{ fontSize: 40, color: "rgb(283,83,75)" }} /></a>
                    <a href='#home' className={styles.socialIcon}><LinkedInIcon style={{ fontSize: 40, color: "rgb(283,83,75)" }} /></a>
                    <a href='#home' className={styles.socialIcon}><TelegramIcon style={{ fontSize: 40, color: "rgb(283,83,75)" }} /></a>
                    <a href='#home' className={styles.socialIcon}><InstagramIcon style={{ fontSize: 40, color: "rgb(283,83,75)" }} /></a>
                </div>
            </div>
            <div className={styles.copyrightDetails}>
                <p className={styles.footerCopyright}>&#169; 2020 copyright all right reserved<br /><span>Designed with ❤️ by amFOSS</span></p>
            </div>
        </div>
    )
}

export default Footer

