import styles from '../styles/PhotoGallery.module.css'
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import { Image } from 'react-bootstrap';

function PhotoGallery() {
    return (
        <div className={styles.photogalleryContainer}>
            <h1 className={styles.photogalleryTitle}>Photo Gallery</h1>
            <div className={styles.photogalleryImages}>
                <Image src="https://source.unsplash.com/1300x1200/?algeria" alt='' layout="intrinsic" width={400} height={300} />
                <Image src="https://source.unsplash.com/1300x1200/?lebanon" alt='' layout="intrinsic" width={400} height={300} />
                <Image src="https://source.unsplash.com/1300x1200/?qatar" alt='' layout="intrinsic" width={400} height={300} />
                <Image src="https://source.unsplash.com/1300x1200/?uae" alt='' layout="intrinsic" width={400} height={300} />
                <Image src="https://source.unsplash.com/1300x1200/?kuwait" alt='' layout="intrinsic" width={400} height={300} />
                <Image src="https://source.unsplash.com/1300x1200/?oman" alt='' layout="intrinsic" width={400} height={300} />
                <Image src="https://source.unsplash.com/1300x1200/?turkey" alt='' layout="intrinsic" width={400} height={300} />
                <Image src="https://source.unsplash.com/1300x1200/?iran" alt='' layout="intrinsic" width={400} height={300} />
                <Image src="https://source.unsplash.com/1300x1200/?jordan" alt='' layout="intrinsic" width={400} height={300} />
                <Image src="https://source.unsplash.com/1300x1200/?Kuwait" alt='' layout="intrinsic" width={400} height={300} />
            </div>
            <button>VIEW MORE<ArrowForwardIosIcon style={{ fontSize: 20 }} /></button>
        </div>
    )
}

export default PhotoGallery

